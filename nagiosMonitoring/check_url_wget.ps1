<#
Script Name:	check_url_wget
Added:			April 21 2016 (Khaled Ghaben)
Updated:        June 28 2016 (Khaled Ghaben)
Version:		v1.1
Authors:		Khaled Ghaben
Function:		Checks connectivity to IP/Hostname on specified port
Arguments:		STRING/REQUIRED	-hostname	(hostname or IP address)
Arguments:		STRING/OPTIONAL	-url	(URL e.g. /index.html)
Arguments:		STRING/REQUIRED	-searchstring	(string to search for in content of url)
Arguments:		INT/REQUIRED	-port	(port number; default 80)
Arguments:		SWITCH/OPTIONAL	-casesensitive	(enforces casesenstive search for searchstring)
Arguments:		SWITCH/OPTIONAL	-ssl	(Appends https or http accordingly)
Arguments:		SWITCH/OPTIONAL	-verbose	(enables additional console output for debugging)
Example:		check_url_wget -hostname yahoo.com -searchstring yahoo
#>

#region Parameters
Param (
	[string] $hostname = $(throw "-hostname is required."),
	[string] $url = "/",
	[string] $searchstring = $(throw "-searchstring is required."),
	[switch] $ssl = $false,
	[string] $username = 0,
	[string] $password,
	[string] $port = 80,
	[switch] $casesensitive = $false,
	[switch] $verbose = $false
	)
#endregion

#region Invoke Webrequest 
$port = ":" + [string]$port
if ($ssl) { $prefix = "https://" }
else { $prefix = "http://" }

$uri = $prefix + $hostname + $port + $url
if($verbose) {Write-Host "[VERBOSE]uri = $uri" -foregroundcolor "yellow"}

try {
	if ($username -ne 0) 	{
		if($verbose) {Write-Host "[VERBOSE]Creating credentials object" -foregroundcolor "yellow"}
		$secpassword = ConvertTo-SecureString $password -AsPlainText -Force
		$creds = New-Object System.Management.Automation.PSCredential ($username,$secpassword)
		$webobj = Invoke-WebRequest -Uri $uri -Credential $creds
	}
	else { $webobj = Invoke-WebRequest -Uri $uri }
}
catch {
	$message = $_.exception.message
	if($verbose) {Write-Host "[VERBOSE]Recieved the following exception message: `r`n[VERBOSE]$message" -foregroundcolor "yellow"}
	Write-Host "UNKNOWN - check_url_wget - UNEXPECTED results, please contact the systems operation team."
	exit 3
}

#endregion

#region Search String
if ($webobj.content -ne $null) {
	if ($casesensitive) { $regex = "(?-i)$searchstring" }
	else { $regex = "(?i)$searchstring" }
	$occurances = ([regex]::Matches($webobj.content,$regex)).count
	if ($occurances -gt 0) {
		$size = $webobj.RawContentLength
		Write-Host "OK - check_url_wget - $uri - Found '$searchstring' $occurances times in $size bytes."
		exit 0
	}
	else {
		Write-Host "CRITICAL - check_url_wget - $uri - '$searchstring' NOT found in output."
		exit 2
	}
}
else {
	Write-Host "UNKNOWN - check_url_wget - UNEXPECTED results, please contact the systems operation team."
	exit 3
}
#endregion