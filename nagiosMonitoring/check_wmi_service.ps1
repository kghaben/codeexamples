<#
Script Name:	check_wmi_service
Added:			April 21 2016 (Khaled Ghaben)
Updated:        June 28 2016 (Khaled Ghaben)
Version:		v1.1
Authors:		Khaled Ghaben
Function:		Checks connectivity to IP/Hostname on specified port
Arguments:		STRING/REQUIRED	-servicename	(name of service)
Arguments:		SWITCH/OPTIONAL	-verbose	(enables additional console output for debugging)
Example:		check_wmi_service -servicename BITS
#>

#region Parameters
Param (
	[string] $servicename = $(throw "-servicename is required."),
	[switch] $verbose = $false
	)
#endregion

#region Get-service

try {
	$serviceobj = Get-Service -Name $servicename -ErrorAction Stop
}
catch {
	$message = $_.exception.message
	if($verbose) {Write-Host "[VERBOSE]Recieved the following exception message: `r`n[VERBOSE]$message" -foregroundcolor "yellow"}
	Write-host "UNKNOWN - check_wmi_service - UNEXPECTED results, please contact the systems operation team."
	exit 3
}
#endregion

#region Status logic
if($serviceobj) {
	$status = $serviceobj.Status
	if ($status -eq "Running") {
		Write-host "OK - check_wmi_service - $servicename service is running."
		exit 0
	}
	elseif ($status -eq "Stopped") {
		Write-host "CRITICAL - check_wmi_service - $servicename service NOT running."
		exit 2
	}
	else {
		if($verbose) {Write-Host "[VERBOSE]Service: $servicename - Status: $status" -foregroundcolor "yellow"}
		Write-host "UNKNOWN - check_wmi_service - UNEXPECTED results, please contact the systems operation team."
		exit 3
	}
}
else {
	$type = $serviceobj.GetType()
	if($verbose) {Write-Host "[VERBOSE]Service object was not set." -foregroundcolor "yellow"}
	Write-host "UNKNOWN - check_wmi_service - UNEXPECTED results, please contact the systems operation team."
	exit 3
}
#endregion