<#
Script Name:	check_port_nc
Added:			April 21 2016 (Khaled Ghaben)
Updated:        June 28 2016 (Khaled Ghaben)
Version:		v1.1
Authors:		Khaled Ghaben
Function:		Checks connectivity to IP/Hostname on specified port
Arguments:		STRING/REQUIRED	-hostname	(hostname or IP address)
Arguments:		INT/REQUIRED	-port	(port number)
Arguments:		SWITCH/OPTIONAL	-verbose	(enables additional console output for debugging)
Example:		check_port_nc -hostname yahoo.com -port 80
#>

#region Parameters
Param (
	[string] $hostname = $(throw "-hostname is required."),
	[int] $port = $(throw "-port is required."),
	[switch] $verbose = $false
	)
#endregion

#region DNS resolution
try {
    $ip = [System.Net.Dns]::GetHostAddresses($hostname) | 
        select-object IPAddressToString -expandproperty  IPAddressToString
    if($ip.GetType().Name -eq "Object[]")
    {
        #If we have several ip's for that address, let's take first one
		$testip = $ip[0]
    }
} catch {
	$message = $_.exception.message
	if($verbose) {Write-Host "[VERBOSE]Recieved the following exception message: `r`n[VERBOSE]$message" -foregroundcolor "yellow"}
	Write-Host "CRITICAL - check_port_nc - Could not resolve hostname: $hostname"
    exit 2
}
#endregion

#region Connection Test Loop
$t = New-Object Net.Sockets.TcpClient
# We use Try\Catch to remove exception info from console if we can't connect

$count = $ip.count
if($verbose) {Write-Host "[VERBOSE]$count IPs found." -foregroundcolor "yellow"}
$loop = 0

Do {
	try
	{
    	$t.Connect($testip,$port)
	} catch {}
	
	if($t.Connected)
	{
		$t.Close()
		Write-Host "OK - check_port_nc - Port $port on $hostname is successful."
		exit 0
	}
	if($verbose) {Write-Host "[VERBOSE]Resolved IP: $testip failed to connect on Port $port." -foregroundcolor "yellow"}
	$loop++
	$testip = $ip[$loop]
} while ($loop -lt $count)
    

Write-Host "CRITICAL - check_port_nc - Port $port on $hostname FAILED check."
exit 2
#endregion