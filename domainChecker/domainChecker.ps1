<#########################################################

Script Name:	Is it up? - Domain Checker
Initial Commit:	Jun 26 2016 (Khaled Ghaben)
Last Updated:	Jun 30 2016 (Khaled Ghaben)
Version:		v1.4
Authors:		Khaled Ghaben (khaled@ghaben.com)

Function:		Checks for success/failure return code 
                from a list of domains by resolving the 
                IP address(es) from DNS.

Features:       > .NET/WPF Application GUI
                > Multi-threaded query utilizing Jobs
                > Checks for all 'Round Robin' IPs
                > Export findings into a CSV file

Known bugs:     > GUI hangs due to single thread
                > searchButton clickable when disabled

To do:          > Utilize runspaces for WPF GUI
                > Create a syncHash to share variables

##########################################################>


#===========================================================================
# Loading and formatting XAML
#===========================================================================
$inputXML = gc $PSScriptRoot\gui.xml      
$inputXML = $inputXML -replace 'mc:Ignorable="d"','' -replace "x:N",'N'  -replace '^<Win.*', '<Window'
[void][System.Reflection.Assembly]::LoadWithPartialName('presentationframework')
[xml]$XAML = $inputXML

#===========================================================================
# Loading XAML object
#===========================================================================
$reader=(New-Object System.Xml.XmlNodeReader $xaml)
try
{
    $Form=[Windows.Markup.XamlReader]::Load( $reader )
}
catch
{
    $xamlError = "Unable to load Windows.Markup.XamlReader.`nDouble-check syntax and ensure .net is installed."
    Write-Host $saveFileError
    [System.Windows.Forms.MessageBox]::Show($xamlError, 'Error', 'OK', 'Error')
}
$xaml.SelectNodes("//*[@Name]") | %{Set-Variable -Name "WPF$($_.Name)" -Value $Form.FindName($_.Name)}
 
#===========================================================================
# Job status tracking function
#===========================================================================
 Function _jobStatus 
{
    Param (
    [Parameter(Mandatory=$true)][int]$itemcount
    )
    Process
    {
        $runningCount = $(Get-Job -State Running | Measure-Object).count
        $completedCount = $(Get-Job -State Completed | Measure-Object).count
        $percent = "{0:N0}" -f ($completedCount / $itemcount * 100)
        Write-Verbose "Completed Count: $completedCount"
        Write-Verbose "Running Count: $runningCount"
        Write-Verbose "Percentage: $percent %"
        
        return [PSCustomObject]@{runningCount = $runningCount; completedCount = $completedCount; percent = $percent}
    }
}

#===========================================================================
# Search button action
#===========================================================================
$WPFsearchButton.Add_Click({
    ## MAXIMUM THREAD COUNT ##
    $maxCount = 10

    ## Search button disabling ##
    $WPFsearchButton.Content = "Processing..."
    $WPFsearchButton.IsEnabled = $false
    
    ## Initialize variables and fields ##
    remove-job *
    $WPFlistView.Items.Clear()
    $itemcount = 0
    

    ## Progress bar initializing ##
    $WPFbar1.Visibility = "Visible"
    $WPFbar1.IsEnabled = $true
    
    ## Item count for progress bar ##
    
    $items = $WPFtextBox.Text -split "," -replace " ",""
    if ($WPFcheckBox.IsChecked -eq $false) { $itemcount = $items.Count }
    else { foreach($item in $items) { $itemcount += $([System.Net.Dns]::GetHostAddresses($item)).count } }
    
    ## Progress bar rendering ##
    $Form.Dispatcher.Invoke( "Render", ([Action``2[[System.Object,mscorlib],[System.Object, mscorlib]]]{ $WPFbar1.Value = 0; } ), $null, $null )
        
    foreach($item in $items) {
        try { $ips = [System.Net.Dns]::GetHostAddresses($item) | select-object IPAddressToString -expandproperty  IPAddressToString -ErrorAction Stop }
        catch { $ips = "Failed to Resolve" }

        foreach($ip in $ips)
		{
	        
            do { 
            Start-Sleep -Milliseconds 50
            $jobstatus = _jobStatus -itemcount $itemcount
            $percentage = $jobstatus.percent
            $completed = $jobstatus.completedCount
            $WPFtextBlock.Text = "$completed of $itemcount Completed"
            $Form.Dispatcher.Invoke( "Render", ([Action``2[[System.Object,mscorlib],[System.Object, mscorlib]]]{ $WPFbar1.Value = $percentage; } ), $null, $null )
            } until ($jobstatus.runningCount -le $maxCount) 
            

            Start-Job -ScriptBlock { 
	            try
	            { 
	                $request = $null 
	                ## Request the URI, and measure how long the response took.
	                $url = $args[0]
					$ip = $args[1]
					$time = (Measure-Command { $request = Invoke-WebRequest -Uri $ip } ).Milliseconds
                    $result = New-Object psobject -Property @{
	                    URL = $url
                        IPAddress = $ip
	                    StatusCode = $request.StatusCode
	                    Description = $request.StatusDescription
	                    ResponseTime = $time
                    }
	                return $result
						

	            }  
	            catch 
	            { 
	                $errmsg = $_.Exception.Response
	                $time = -1
	                $result = New-Object psobject -Property @{
	                    URL = $args[0]
                        IPAddress = $ip
	                    StatusCode = $errmsg
	                    Description = "Failure"
	                    ResponseTime = $time
                    }
	                return $result
	            }   
	        } -ArgumentList $item,$ip
		    
            if($WPFcheckBox.IsChecked -eq $false) { break }
        }
    }
    
    

    Do
    {
        Start-Sleep -Milliseconds 200
        $jobstatus = _jobStatus -itemcount $itemcount
        $percentage = $jobstatus.percent
        $completed = $jobstatus.completedCount
        $WPFtextBlock.Text = "$completed of $itemcount Completed"
        $WPFbar1.Value = $jobstatus.percent

    } until ($jobstatus.runningCount -eq 0)

    $WPFbar1.Value = 100
    Receive-Job * | select -Property URL,IPAddress,StatusCode,Description,ResponseTime |  % {$WPFlistView.AddChild($_)}
    $WPFsearchButton.IsEnabled = $true
    $WPFsearchButton.Content = "Search"
    $WPFexportButton.IsEnabled = $true
})
 
#===========================================================================
# Save File Dialog box function for CSV export
#===========================================================================
Function _saveFileDialog
{
    $saveFileDialog = New-Object System.Windows.Forms.SaveFileDialog
    $saveFileDialog.DefaultExt = 'csv'
    $saveFileDialog.Filter = "CSV files (*.csv)|*.csv"
    $saveFileDialog.InitialDirectory = $PSScriptRoot
    $saveFileDialog.ShowDialog()
    $saveFileDialog.FileName
}

$WPFexportButton.Add_Click({
    $filepath = _saveFileDialog
    try 
    {
        $WPFlistView.Items | Export-Csv $filepath[1] -NoTypeInformation -ErrorAction Stop

    }
    catch
    {
        $saveFileError = "Failed to save file:`n" + $_.Exception.Message
        Write-Host $saveFileError
        [System.Windows.Forms.MessageBox]::Show($saveFileError, 'Error', 'OK', 'Error')
    }
})



$Form.ShowDialog() | out-null