Checks for success/failure return code from a list of domains by resolving the IP address(es) from DNS.
Leverages WPF Application GUI, Multi-threaded queries through Powershell jobs, and CSV file export.

>>> domainChecker.ps1 and gui.xml are BOTH REQUIRED for execution. <<<